Promise.all([
    fetch('https://ajax.test-danit.com/api/json/users'),
    fetch('https://ajax.test-danit.com/api/json/posts')
]).then((responses) => {
    return Promise.all(responses.map((response) => {
        return response.json();
    }));
}).then(([users, posts]) => {
    const getInfo = new GetInfo();
    getInfo.getAllInfo(users, posts)
}).catch((error) => {
    console.log(error);
});

class GetInfo {
    getAllInfo(users, posts) {
        users.forEach(({
            name,
            email,
            id
        }) => {
            posts.forEach(({
                id: postid,
                title,
                body,
                userId
            }) => {
                if (id === userId) {
                    const card = new Card(name, email, title, body, postid);
                    document.getElementById("root").append(card.render());
                }
            })
        })
    }
}

class Card {
    constructor(name, email, title, body, postid) {
        this.name = name,
            this.email = email,
            this.title = title,
            this.body = body,
            this.postid = postid
    }

    createElement(tagName, className) {
        const element = document.createElement(tagName);
        element.className = className;
        return element;
    }


    render() {
        const div = this.createElement("div", "div-wrapper");
        div.id = `div-wrapper_${this.postid}`;
        const divInfo = this.createElement("div", "div-info");
        const name = this.createElement("p", "name");
        const email = this.createElement("a", "email");
        const title = this.createElement("p", "title");
        const body = this.createElement("p", "body");
        const button = this.createElement("button", "button");
        name.textContent = this.name;
        email.textContent = this.email;
        title.textContent = this.title;
        body.textContent = this.body;
        button.textContent = "Delete";
        divInfo.append(name, email)
        div.append(divInfo, title, body, button);
        button.addEventListener("click", () => {
            this.deletePost()
        })
        return div
    }


    deletePost() {   
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postid}`, {
                method: 'DELETE',
            })
            .then(response => {
                if (response.status === 200) {
                    document.getElementById(`div-wrapper_${this.postid}`).remove();
                }
            })
    
}

}
